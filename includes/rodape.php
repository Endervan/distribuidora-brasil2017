
<div class="container-fluid rodape top50">
	<div class="row">




		<div class="container">
			<div class="row">

				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->
				<div class="col-3 top65">
					<a href="<?php echo Util::caminho_projeto() ?>/"  title="início">
						<img src="<?php echo Util::caminho_projeto() ?>/imgs/logo_rodape.png" alt="início" />
					</a>
				</div>
				<!-- ======================================================================= -->
				<!-- LOGO    -->
				<!-- ======================================================================= -->


				<div class="col-2 menu_rodape top40">
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
					<ul class="nav flex-column ">
						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == ""){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/">HOME
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "empresa"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/empresa">A EMPRESA
							</a>
						</li>

						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "dicas" or Url::getURL( 0 ) == "dica"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/dicas">DICAS
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "produtos" or Url::getURL( 0 ) == "produto"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/produtos">PRODUTOS
							</a>
						</li>


						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "fale-conosco"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/">FALE CONOSCO
							</a>
						</li>


						<li class="nav-item">
							<a class="nav-link <?php if(Url::getURL( 0 ) == "trabalhe-conosco"){ echo "active"; } ?>"
								href="<?php echo Util::caminho_projeto() ?>/">TRABALHE CONOSCO
							</a>
						</li>
					</ul>
					<!-- ======================================================================= -->
					<!-- MENU    -->
					<!-- ======================================================================= -->
				</div>

				<div class="col-3 categorias_rodape top40">
					<!-- ======================================================================= -->
					<!--CATEGORIAS    -->
					<!-- ======================================================================= -->
					<ul class="nav flex-column ">
						<?php
						$result = $obj_site->select("tb_categorias_produtos");
						if(mysql_num_rows($result) > 0){
							while($row = mysql_fetch_array($result)){
								$i=0;
								?>
								<li class="nav-item">
									<a class="nav-link text-uppercase" href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>">
										<?php Util::imprime($row[titulo]); ?>
									</a>
								</li>
								<?php
								if ($i==0) {
									echo "<div class='clearfix'>  </div>";
								}else {
									$i++;
								}
							}
						}
						?>
					</ul>
					<!-- ======================================================================= -->
					<!--CATEGORIAS    -->
					<!-- ======================================================================= -->
				</div>


				<!-- ======================================================================= -->
				<!-- ENDERECO E TELEFONES    -->
				<!-- ======================================================================= -->
				<div class="col-3 top40 endereco_rodape">
					<div class="">
						<p class="bottom15">
							<i class="fa fa-phone right5"></i>
							<span><?php Util::imprime($config[ddd1]); ?></span><?php Util::imprime($config[telefone1]); ?>

							<b class="left5"></b>

							<?php if (!empty($config[telefone2])) { ?>
								<i class="fa fa-whatsapp right5"></i>
								<span><?php Util::imprime($config[ddd2]); ?></span><?php Util::imprime($config[telefone2]); ?>
							<?php } ?>

							<?php if (!empty($config[telefone3])) { ?>
								<span><?php Util::imprime($config[ddd3]); ?></span><?php Util::imprime($config[telefone3]); ?>
							<?php } ?>

							<?php if (!empty($config[telefone4])) { ?>
								<span><?php Util::imprime($config[ddd4]); ?></span><?php Util::imprime($config[telefone4]); ?>
							<?php } ?>
						</p>
					</div>

						<div class="media">
							<i class="fa fa-home d-flex align-self-center mr-3" aria-hidden="true"></i>
							<div class="media-body">
								<p class="mt-0"><?php Util::imprime($config[endereco]); ?></p>
							</div>
						</div>

				</div>
				<!-- ======================================================================= -->
				<!-- ENDERECO E TELEFONES    -->
				<!-- ======================================================================= -->





				<!-- ======================================================================= -->
				<!-- redes sociais    -->
				<!-- ======================================================================= -->
				<div class="col-1 redes text-right top30">
					<?php if ($config[google_plus] != "") { ?>
						<a href="<?php Util::imprime($config[google_plus]); ?>" title="Google Plus" target="_blank" >
							<i class="fa fa-google-plus-official fa-2x right15"></i>
						</a>
					<?php } ?>

					<?php if ($config[facebook] != "") { ?>
						<a href="<?php Util::imprime($config[facebook]); ?>" title="facebook" target="_blank" >
							<i class="fa fa-facebook-official fa-2x top20 right15"></i>
						</a>
					<?php } ?>

					<?php if ($config[instagram] != "") { ?>
						<a href="<?php Util::imprime($config[instagram]); ?>" title="instagram" target="_blank" >
							<i class="fa fa-instagram fa-2x top20 right15"></i>
						</a>
					<?php } ?>

					<a href="http://www.homewebbrasil.com.br" target="_blank">
						<img class="top20" src="<?php echo Util::caminho_projeto() ?>/imgs/logo_homeweb.png"  alt="">
					</a>

				</div>
				<!-- ======================================================================= -->
				<!-- redes sociais    -->
				<!-- ======================================================================= -->




			</div>
		</div>
	</div>
</div>

<div class="container-fluid">
	<div class="row rodape-preto lato">
		<div class="col-12 text-center top10 bottom10">
			<h5>© Copyright DISTRIBUIDORA BRASIL</h5>
		</div>
	</div>
</div>
