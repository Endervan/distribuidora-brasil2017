<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 7);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",7) ?>
<style>
.bg-interna{
  background:  #f2f2f2 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top120">
      <div class="col-12 text-center titulo_emp">
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
        <div class="top25">
          <h2 class="Lato">SEMPRE QUE PRECISAR</h2>
          <h4><span>FALE CONOSCO</span></h4>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!--  titulo -->
<!-- ======================================================================= -->





<!-- ======================================================================= -->
<!-- fale conosco -->
<!-- ======================================================================= -->
<div class="container top65">
  <form class="FormContatos" role="form" method="post" enctype="multipart/form-data">
    <div class="row ">

      <!--  ==============================================================  -->
      <!-- FORMULARIO CONTATOS-->
      <!--  ==============================================================  -->
      <div class="col-2">  </div>
      <div class="col-8 fundo_formulario">

        <div class="form-row">
          <div class="col">
            <div class="form-group icon_form">
              <input type="text" name="nome" class="form-control fundo-form form-control-lg input-lg" placeholder="NOME">
              <span class="fa fa-user form-control-feedback"></span>
            </div>
          </div>

          <div class="col">
            <div class="form-group  icon_form">
              <input type="text" name="email" class="form-control fundo-form form-control-lg input-lg" placeholder="E-MAIL">
              <span class="fa fa-envelope form-control-feedback"></span>
            </div>
          </div>
        </div>

        <div class="form-row">
          <div class="col">
            <div class="form-group  icon_form">
              <input type="text" name="telefone" class="form-control fundo-form form-control-lg input-lg" placeholder="TELEFONE">
              <span class="fa fa-phone form-control-feedback"></span>
            </div>
          </div>

          <div class="col">
            <div class="form-group  icon_form">
              <input type="text" name="assunto" class="form-control fundo-form form-control-lg input-lg" placeholder="ASSUNTO">
              <span class="fa fa-star form-control-feedback"></span>
            </div>
          </div>
        </div>


        <div class="form-row">
          <div class="col">
            <div class="form-group icon_form">
              <textarea name="mensagem" cols="25" rows="5" class="form-control form-control-lg fundo-form" placeholder="MENSAGEM"></textarea>
              <span class="fa fa-pencil form-control-feedback"></span>
            </div>
          </div>
        </div>


        <div class="col-12 text-center  top15 bottom25">
          <button type="submit" class="btn btn_formulario" name="btn_contato">
            ENVIAR
          </button>
        </div>


      </div>

  </div>
  </form>
  <!--  ==============================================================  -->
  <!-- FORMULARIO CONTATOS-->
  <!--  ==============================================================  -->
</div>




<div class='container top150'>
  <div class="row mapa">
    <div class="col-4">
      <h1>
        <span>
          <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_fale.png" alt="">
          CENTRAL DE ATENDIMENTO
        </span>
      </h1>
      <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_fale.png" alt="">
      <h1 class="left20 top10"><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></h1>
      <h1 class="left20 top10"><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h1>

      <div class="top35">
        <h1>
          <span>
            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_local.png" alt="">
            <b>COMO CHEGAR</b>
          </span>
        </h1>
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_fale.png" alt="">
        <P class="left20"><?php Util::imprime($config[endereco]); ?></P>
      </div>
    </div>

    <div class="col">
      <!-- ======================================================================= -->
      <!-- mapa   -->
      <!-- ======================================================================= -->
      <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="429" frameborder="0" style="border:0" allowfullscreen></iframe>

      <!-- ======================================================================= -->
      <!-- mapa   -->
      <!-- ======================================================================= -->
    </div>
  </div>
</div>



<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>



<?php
//  VERIFICO SE E PARA ENVIAR O EMAIL
if(isset($_POST[nome]))
{
  $texto_mensagem = "
  Assunto: ".($_POST[assunto])." <br />
  Nome: ".($_POST[nome])." <br />
  Email: ".($_POST[email])." <br />
  assunto: ".($_POST[assunto])." <br />
  Celular: ".($_POST[celular])." <br />

  Mensagem: <br />
  ".(nl2br($_POST[mensagem]))."
  ";

  if (Util::envia_email($config[email_fale_conosco], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email])) {
    Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]), $_POST[email]);
    Util::alert_bootstrap("Obrigado por entrar em contato.");
    unset($_POST);
  }else{
    Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
  }

}

?>



<script>
$(document).ready(function() {
  $('.FormContatos').bootstrapValidator({
    message: 'This value is not valid',
    feedbackIcons: {
      valid: 'fa fa-check',
      invalid: 'fa fa-remove',
      validating: 'fa fa-refresh'
    },
    fields: {
      nome: {
        validators: {
          notEmpty: {
            message: 'Insira seu nome.'
          }
        }
      },
      mensagem: {
        validators: {
          notEmpty: {
            message: 'Insira sua Mensagem.'
          }
        }
      },
      email: {
        validators: {
          notEmpty: {
            message: 'Informe um email.'
          },
          emailAddress: {
            message: 'Esse endereço de email não é válido'
          }
        }
      },
      telefone: {
        validators: {
          notEmpty: {
            message: 'Por favor informe seu numero!.'
          },
          phone: {
            country: 'BR',
            message: 'Informe um telefone válido.'
          }
        },
      },
      assunto: {
        validators: {
          notEmpty: {

          }
        }
      }
    }
  });
});
</script>
