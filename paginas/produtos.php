<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 3);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",2) ?>
<style>
.bg-interna{
  background: #f2f2f2 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top90">
      <div class="col-12">
        <h4>CONHEÇA NOSSOS</h4>
        <h4><span>PRODUTOS</span></h4>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!--  titulo -->
<!-- ======================================================================= -->


<div class="container top75">
  <div class="row categorias_produtos">
    <div class="col-12">

      <!-- ======================================================================= -->
      <!--CATEGORIAS    -->
      <!-- ======================================================================= -->
      <ul class="nav ">
        <?php
        $result = $obj_site->select("tb_categorias_produtos");
        if(mysql_num_rows($result) > 0){
          while($row = mysql_fetch_array($result)){
            $i=0;
            ?>

            <li class=" text-center top20 nav-item">
              <?php $url = Url::getURL(1); ?>
              <a class="nav-link <?php if($url == "$row[url_amigavel]"){ echo 'active'; } ?>  text-uppercase" href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <h4><?php Util::imprime($row[titulo]); ?></h4>
              </a>
            </li>
            <?php
            if ($i==2) {
              echo "<div class='clearfix'>  </div>";
            }else {
              $i++;
            }
          }
        }
        ?>
      </ul>
      <!-- ======================================================================= -->
      <!--CATEGORIAS    -->
      <!-- ======================================================================= -->
    </div>
  </div>
</div>




<!-- ======================================================================= -->
<!-- PRODUTOS    -->
<!-- ======================================================================= -->
<div class="container top90">
  <div class="row">

    <?php


    $url = Url::getURL(1);

    //  FILTRA AS CATEGORIAS
    if (isset( $url )) {
      $id_categoria = $obj_site->get_id_url_amigavel("tb_categorias_produtos", "idcategoriaproduto", $url);
      $complemento .= "AND id_categoriaproduto = '$id_categoria' ";
    }

    //  FILTRA PELO TITULO
    if(isset($_POST[busca_produtos])):
      $complemento .= "AND titulo LIKE '%$_POST[busca_produtos]%'";
    endif;

    ?>

    <?php

    //  busca os produtos sem filtro
    $result = $obj_site->select("tb_produtos", $complemento);

    if(mysql_num_rows($result) == 0){
      echo "<h2 class='btn_orcamento w-100 clearfix text-white' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
    }else{
      ?>
      <div class="col-12 total-resultado-busca bottom10">
        <h1><span><?php echo mysql_num_rows($result) ?></span> PRODUTO(S) ENCONTRADO(S) . </h1>
      </div>

      <?php
      require_once('./includes/lista_produtos.php');

    }
    ?>

  </div>
</div>
<!-- ======================================================================= -->
<!-- PRODUTOS    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
