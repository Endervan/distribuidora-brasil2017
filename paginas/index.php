<?php
// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 1);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>


</head>
<body>



  <div class="topo-site">
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
    <?php require_once('./includes/topo.php') ?>
    <!-- ======================================================================= -->
    <!-- topo    -->
    <!-- ======================================================================= -->
  </div>



  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->
  <div class="container-fluid relative">
    <div class="row">

      <div id="container_banner">

        <div id="content_slider">
          <div id="content-slider-1" class="contentSlider rsDefault">

            <?php
            $result = $obj_site->select("tb_banners", "AND tipo_banner = 1 ORDER BY rand() LIMIT 4");

            if (mysql_num_rows($result) > 0) {
              while ($row = mysql_fetch_array($result)) {
                ?>
                <!-- ITEM -->
                <div>
                  <?php
                  if ($row[url] == '') {
                    ?>
                    <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    <?php
                  }else{
                    ?>
                    <a href="<?php Util::imprime($row[url]) ?>">
                      <img class="rsImg" src="./uploads/<?php Util::imprime($row[imagem]) ?>" data-rsw="1920" data-rsh="996" alt=""/>
                    </a>
                    <?php
                  }
                  ?>

                </div>
                <!-- FIM DO ITEM -->
                <?php
              }
            }
            ?>

          </div>
        </div>

      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- SLIDER    -->
  <!-- ======================================================================= -->


  <!-- ======================================================================= -->
  <!--EMPRESA HOME   -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row empresa_home">
      <div class="col"></div>
      <div class="col text-center">
        <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 1);?>
        <div class="bg_empresa_home">
          <h1><?php Util::imprime($row[titulo]); ?></h1>
          <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_parceiro.png" alt="" >
        </div>

      </div>
      <div class="col"> </div>

      <div class="col-12">
        <div class="top50">
          <p><?php Util::imprime($row[descricao]); ?></p>
        </div>

      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!--EMPRESA HOME   -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--EMPRESA FACILIDADES   -->
  <!-- ======================================================================= -->
  <div class="container top45">
    <div class="row">

      <div class="col-4">
        <div class="facildades ">
          <div class="row">
            <div class="col-3 top15">
              <img src="<?php echo Util::caminho_projeto(); ?>/imgs/falicidades01.png" alt="">
            </div>
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
            <div class="col-9">
              <div class="">
                <h3><span><?php Util::imprime($row[titulo]); ?></span></h3>
              </div>
              <div class=" bottom20">
                <p><?php Util::imprime($row[descricao],80); ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-4">
        <div class="facildades ">
          <div class="row">
            <div class="col-3 top15">
              <img src="<?php echo Util::caminho_projeto(); ?>/imgs/falicidades02.png" alt="">
            </div>
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4);?>
            <div class="col-9">
              <div class="">
                <h3><span><?php Util::imprime($row[titulo]); ?></span></h3>
              </div>
              <div class=" bottom20">
                <p><?php Util::imprime($row[descricao],80); ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-4">
        <div class="facildades ">
          <div class="row">
            <div class="col-3 top15">
              <img src="<?php echo Util::caminho_projeto(); ?>/imgs/falicidades03.png" alt="">
            </div>
            <div class="col-9">
              <div class="">
                <h3><span>CENTRAL DE ATENDIMENTO</span></h3>
              </div>
              <div class=" bottom20">
                <p>

                  <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
                  <?php if (!empty($config[telefone2])): ?>
                    / <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
                  <?php endif; ?>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>



      <div class="col-4 top50">
        <div class="facildades ">
          <div class="row">
            <div class="col-3 top15">
              <img src="<?php echo Util::caminho_projeto(); ?>/imgs/falicidades05.png" alt="">
            </div>
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5);?>
            <div class="col-9">
              <div class="">
                <h3><span><?php Util::imprime($row[titulo]); ?></span></h3>
              </div>
              <div class=" bottom20">
                <p><?php Util::imprime($row[descricao],80); ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-4 top50">
        <div class="facildades ">
          <div class="row">
            <div class="col-3 top15">
              <img src="<?php echo Util::caminho_projeto(); ?>/imgs/falicidades06.png" alt="">
            </div>
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 6);?>
            <div class="col-9">
              <div class="">
                <h3><span><?php Util::imprime($row[titulo]); ?></span></h3>
              </div>
              <div class=" bottom20">
                <p><?php Util::imprime($row[descricao],130); ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="col-4 top50">
        <div class="facildades ">
          <div class="row">
            <div class="col-3 top15">
              <img src="<?php echo Util::caminho_projeto(); ?>/imgs/falicidades01.png" alt="">
            </div>
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4);?>
            <div class="col-9">
              <div class="">
                <h3><span><?php Util::imprime($row[titulo]); ?></span></h3>
              </div>
              <div class=" bottom20">
                <p><?php Util::imprime($row[descricao],80); ?></p>
              </div>
            </div>
          </div>
        </div>
      </div>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!--EMPRESA FACILIDADES   -->
  <!-- ======================================================================= -->






  <div class="container ">
    <div class="row">
      <div class="col-12 top50">
        <h3>NOSSOS PARCEIROS</h3>
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_parceiros.png" alt="" >
      </div>


      <div class="col-12">
        <!-- ======================================================================= -->
        <!-- slider clientes -->
        <!-- ======================================================================= -->
        <?php require_once('./includes/slider_clientes.php') ?>
        <!-- ======================================================================= -->
        <!-- slider clientes -->
        <!-- ======================================================================= -->
      </div>
    </div>
  </div>



  <!-- ======================================================================= -->
  <!-- CATEGORIAS HOME -->
  <!-- ======================================================================= -->
  <div class="container top70">
    <div class="row lista_categorias">
      <div class="col-12 lato_black text-center">
        <div id="slider" class="flexslider">
          <ul class="slides">

            <?php
            $i=0;
            $result = $obj_site->select("tb_categorias_produtos");
            if (mysql_num_rows($result) > 0) {

              while ($row = mysql_fetch_array($result)) {
                ?>
                <li class="<?php if($i++ == 0){ echo 'active'; }?>">
                  <a href="<?php echo Util::caminho_projeto() ?>/produtos/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>" >
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 269, 494, array("class"=>"w-100", "alt"=>"$row[titulo]")) ?>
                  </a>
                </li>
                <?php
              }

            }
            ?>

          </ul>
        </div>
      </div>

    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- CATEGORIAS HOME -->
  <!-- ======================================================================= -->



  <!--  ==============================================================  -->
  <!--   LISTAGEM DOS PRODUTOS -->
  <!--  ==============================================================  -->
  <div class="container">
    <div class="row top55">
      <div class="col-12 lato_black">
        <div class="lista-produtos-index">

          <!-- Nav tabs -->
          <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
              <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-expanded="true">
                EM DESTAQUES
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-expanded="true">
                OS MAIS VENDIDOS
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-item nav-link" id="nav-messege-tab" data-toggle="tab" href="#nav-messege" role="tab" aria-controls="nav-messege" aria-expanded="true">
                OS MAIS VISITADOS
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="<?php echo Util::caminho_projeto() ?>/produtos">VER TODOS OS PRODUTOS</a>
            </li>

            <li class="nav-item procura">
              <!--  ==============================================================  -->
              <!--BARRA PESQUISAS-->
              <!--  ==============================================================  -->
              <form action="<?php echo Util::caminho_projeto() ?>/produtos/" method="post">
                <div class="input-group input-group-lg">
                  <input type="text" class="form-control" name="busca_produtos" placeholder="ENCONTRE O PRODUTO QUE DESEJA">
                  <span class="input-group-btn">
                    <button class="btn border-0" type="submit"><span class="fa fa-search"></span></button>
                  </span>
                </div>
              </form>
              <!--  ==============================================================  -->
              <!--BARRA PESQUISAS-->
              <!--  ==============================================================  -->
            </li>
          </ul>
        </div>
      </div>




      <!-- Tab panes -->
      <div class=" tab-content col-12 padding0" id="myTabContent">
        <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">

          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->
          <?php include('./includes/lista_produtos_home.php') ?>
          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->


        </div>
        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">

          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->
          <?php include('./includes/lista_produtos_home.php') ?>
          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->

        </div>


        <div class="tab-pane fade" id="nav-messege" role="tabpanel" aria-labelledby="nav-messege-tab">
          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->
          <?php include('./includes/lista_produtos_home.php') ?>
          <!-- ======================================================================= -->
          <!-- LISTA PRODUTO HOME    -->
          <!-- ======================================================================= -->
        </div>
      </div>


    </div>
  </div>
  <!--  ==============================================================  -->
  <!--   LISTAGEM DOS PRODUTOS -->
  <!--  ==============================================================  -->






  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->

</body>

</html>

<!-- slider JS files -->
<script  src="<?php echo Util::caminho_projeto() ?>/js/jquery-1.8.0.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/royalslider.css" rel="stylesheet">
<script src="<?php echo Util::caminho_projeto() ?>/js/jquery.royalslider.min.js"></script>
<link href="<?php echo Util::caminho_projeto() ?>/css/rs-minimal-white.css" rel="stylesheet">



<script>
jQuery(document).ready(function($) {
  // Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
  // it's recommended to disable them when using autoHeight module
  $('#content-slider-1').royalSlider({
    autoHeight: true,
    arrowsNav: true,
    arrowsNavAutoHide: false,
    keyboardNavEnabled: true,
    controlNavigationSpacing: 0,
    controlNavigation: 'tabs',
    autoScaleSlider: false,
    arrowsNavAutohide: true,
    arrowsNavHideOnTouch: true,
    imageScaleMode: 'none',
    globalCaption:true,
    imageAlignCenter: false,
    fadeinLoadedSlide: true,
    loop: false,
    loopRewind: true,
    numImagesToPreload: 6,
    keyboardNavEnabled: true,
    usePreloader: false,
    autoPlay: {
      // autoplay options go gere
      enabled: true,
      pauseOnHover: true
    }

  });
});
</script>





<?php require_once('./includes/js_css.php') ?>

<script type="text/javascript">
$(window).load(function() {
  $('.flexslider').flexslider({
    animation: "slide",
    animationLoop: false,
    itemWidth: 300,
    itemMargin: 0,
    inItems: 1,
    maxItems: 3
  });
});
</script>
