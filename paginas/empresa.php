<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",1) ?>
<style>
.bg-interna{
  background:  #f2f2f2 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top115">
      <div class="col-12 titulo_emp">
        <div class="top25">
          <h2 class="Lato">SAIBA MAIS SOBRE A </h2>
          <h4><span>DISTRIBUIDORA BRASIL</span></h4>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!--  titulo -->
<!-- ======================================================================= -->




<div class="container top170">
  <div class="row empresa ">

    <!-- ======================================================================= -->
    <!-- conheca nossa empresa  -->
    <!-- ======================================================================= -->
    <div class="col-12 top20 bottom40">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
      <div class="desc_empresa">
        <p><?php Util::imprime($row[descricao]); ?></p>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!-- conheca nossa empresa  -->
    <!-- ======================================================================= -->
  </div>
</div>


<div class="container">
  <div class="row">

    <div class="card-columns">

      <div class="text-center facildades_empresa">
        <div class="card">
          <img class="pg20" src="<?php echo Util::caminho_projeto(); ?>/imgs/falicidades01.png" alt="">
          <div class="card-body">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
            <div class="">
              <h3><span><?php Util::imprime($row[titulo]); ?></span></h3>
            </div>
            <div class="top30 bottom20">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>
          </div>
        </div>
      </div>

      <div class="text-center facildades_empresa">
        <div class="card">
          <img class="pg20" src="<?php echo Util::caminho_projeto(); ?>/imgs/falicidades02.png" alt="">
          <div class="card-body">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4);?>
            <div class="">
              <h3><span><?php Util::imprime($row[titulo]); ?></span></h3>
            </div>
            <div class="top30 bottom20">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>
          </div>
        </div>
      </div>

      <div class="text-center facildades_empresa">
        <div class="card">
          <img class="pg20" src="<?php echo Util::caminho_projeto(); ?>/imgs/falicidades06.png" alt="">
          <div class="card-body">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 7);?>
            <div class="">
              <h3><span><?php Util::imprime($row[titulo]); ?></span></h3>
            </div>
            <div class="top30 bottom20">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>
          </div>
        </div>
      </div>


      <div class="text-center facildades_empresa">
        <div class="card">
          <img class="pg20" src="<?php echo Util::caminho_projeto(); ?>/imgs/falicidades05.png" alt="">
          <div class="card-body">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5);?>
            <div class="">
              <h3><span><?php Util::imprime($row[titulo]); ?></span></h3>
            </div>
            <div class="top30 bottom20">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>
          </div>
        </div>
      </div>

      <div class="text-center facildades_empresa">
        <div class="card">
          <img class="pg20" src="<?php echo Util::caminho_projeto(); ?>/imgs/falicidades04.png" alt="">
          <div class="card-body">
            <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 6);?>
            <div class="">
              <h3><span><?php Util::imprime($row[titulo]); ?></span></h3>
            </div>
            <div class="top30 bottom20">
              <p><?php Util::imprime($row[descricao]); ?></p>
            </div>
          </div>
        </div>
      </div>


    </div>


  </div>
</div>


<div class='container top150'>
  <div class="row mapa">
    <div class="col-4">
      <h1>
        <span>
          <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_fale.png" alt="">
          CENTRAL DE ATENDIMENTO
        </span>
      </h1>
      <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_fale.png" alt="">
      <h1 class="left20 top10"><?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?></h1>
      <h1 class="left20 top10"><?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?></h1>

      <div class="top35">
        <h1>
          <span>
            <img src="<?php echo Util::caminho_projeto(); ?>/imgs/icon_local.png" alt="">
            <b>COMO CHEGAR</b>
          </span>
        </h1>
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_fale.png" alt="">
        <P class="left20"><?php Util::imprime($config[endereco]); ?></P>
      </div>
    </div>

    <div class="col">
      <!-- ======================================================================= -->
      <!-- mapa   -->
      <!-- ======================================================================= -->
      <iframe src="<?php Util::imprime($config[src_place]); ?>" width="100%" height="429" frameborder="0" style="border:0" allowfullscreen></iframe>

      <!-- ======================================================================= -->
      <!-- mapa   -->
      <!-- ======================================================================= -->
    </div>
  </div>
</div>


<div class="container ">
  <div class="row">
    <div class="col-12 top50">
      <h3>NOSSOS PARCEIROS</h3>
      <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_parceiros.png" alt="" >
    </div>


    <div class="col-12">
      <!-- ======================================================================= -->
      <!-- slider clientes -->
      <!-- ======================================================================= -->
      <?php require_once('./includes/slider_clientes.php') ?>
      <!-- ======================================================================= -->
      <!-- slider clientes -->
      <!-- ======================================================================= -->
    </div>
  </div>
</div>


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
