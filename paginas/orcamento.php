<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

//  EXCLUI UM ITEM
if(isset($_GET[action]))
{
  //  SELECIONO O TIPO
  switch($_GET[tipo])
  {
    case "produto":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_produtos][$id]);
    sort($_SESSION[solicitacoes_produtos]);
    break;
    case "servico":
    $id = $_GET[id];
    unset($_SESSION[solicitacoes_servicos][$id]);
    sort($_SESSION[solicitacoes_servicos]);
    break;
    case "piscina_vinil":
    $id = $_GET[id];
    unset($_SESSION[piscina_vinil][$id]);
    sort($_SESSION[piscina_vinil]);
    break;
  }

}


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",8) ?>
<style>
.bg-interna{
  background: url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top30">
      <div class="col-xs-12 titulo_emp">
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
        <div class="top25">
          <h2>SERÁ UM PRAZER ATENDÊ-LO</h2>
          <h4><span>ENVIE SEU ORCAMENTO</span></h4>
        </div>
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_personalizada.png" alt="">
      </div>
    </div>
  </div>
</div>
</div>
<!-- ======================================================================= -->
<!--  titulo -->
<!-- ======================================================================= -->




<div class="container">
  <form class="FormContatos" role="form" method="post" enctype="multipart/form-data">

    <div class="row top85">

      <!-- ======================================================================= -->
      <!-- CARRINHO  -->
      <!-- ======================================================================= -->
      <div class="col-5  top20 tabela_carrinho ">
        <h3 class="">ITENS SELECIONADOS (<?php echo count($_SESSION[solicitacoes_produtos])/*+count($_SESSION[solicitacoes_servicos]);*/ ?>)</h3>
        <table class="table bg_branco table-condensed col-12 ">
          <tbody>

            <?php
            if(count($_SESSION[solicitacoes_produtos]) > 0){
              for($i=0; $i < count($_SESSION[solicitacoes_produtos]); $i++){
                $row = $obj_site->select_unico("tb_produtos", "idproduto", $_SESSION[solicitacoes_produtos][$i]);
                ?>
                <tr>
                  <td>
                    <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 98, 65, array("class"=>"img-rounded", "alt"=>"$row[titulo]")) ?>
                  </td>
                  <td class="col-12"><?php Util::imprime($row[titulo]); ?></td>
                  <td class="text-center">
                    QTD.<br>
                    <input type="number" min=1 class="input-lista-prod-orcamentos" name="qtd[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
                    <input name="idproduto[]" type="hidden" value="<?php echo $row[0]; ?>"  />
                    <div class="help-block with-errors"></div>
                  </td>
                  <td class="text-center">
                    <a href="?action=del&id=<?php echo $i; ?>&tipo=produto" data-toggle="tooltip" data-placement="top" title="Excluir">
                      <i class="fa fa-times-circle fa-2x top20"></i>
                    </a>
                  </td>
                </tr>
                <?php
              }
            }
            ?>

            <?php /*
            if(count($_SESSION[solicitacoes_servicos]) > 0){
            for($i=0; $i < count($_SESSION[solicitacoes_servicos]); $i++){
            $row = $obj_site->select_unico("tb_servicos", "idservico", $_SESSION[solicitacoes_servicos][$i]);
            ?>
            <tr>
            <td>
            <img width="60" height="60"class="media-object" src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="" />
            <!-- <?php //$obj_site->redimensiona_imagem("../uploads/$row[imagem]", 60, 49, array("class"=>"", "alt"=>"$row[titulo]")) ?> -->
            </td>
            <td class="col-12"><?php Util::imprime($row[titulo]); ?></td>
            <td class="text-center">
            QTD.<br>
            <input type="number" min=1 class="input-lista-prod-orcamentos" name="qtd_servico[]" value="1" data-toggle="tooltip" data-placement="top" title="Digite a quantidade desejada">
            <input name="idservico[]" type="hidden" value="<?php echo $row[0]; ?>"  />
            </td>
            <td class="text-center">
            <a href="?action=del&id=<?php echo $i; ?>&tipo=servico" data-toggle="tooltip" data-placement="top" title="Excluir">
            <i class="fa fa-times-circle fa-2x top20"></i>
            </a>
            </td>
            </tr>
            <?php
          }
        }
        */?>

      </table>
    </div>

    <!-- ======================================================================= -->
    <!-- CARRINHO  -->
    <!-- ======================================================================= -->



    <!-- ======================================================================= -->
    <!-- FORMULARIO  -->
    <!-- ======================================================================= -->
    <div class="col-7">
      <div class="fundo_formulario">
        <h3 class="top20">CONFIRME SEUS DADOS</h3>


          <div class="form-row">
            <div class="form-group col icon_form">
              <input type="text" name="nome" class="form-control fundo-form form-control-lg" placeholder="NOME"  required>
              <span class="fa fa-user form-control-feedback"></span>
            </div>
            <div class="form-group col icon_form">
              <input type="text" name="email" class="form-control fundo-form form-control-lg"  placeholder="EMAIL" required>
              <span class="fa fa-envelope form-control-feedback"></span>
            </div>
          </div>

          <div class="form-row">
            <div class="form-group col icon_form">
              <input type="text" name="telefone" class="form-control fundo-form form-control-lg" placeholder="TELEFONE"  required>
              <span class="fa fa-phone form-control-feedback"></span>
            </div>
            <div class="form-group col icon_form">
              <input type="text" name="localidade" class="form-control fundo-form form-control-lg"  placeholder="LOCALIDADE" required>
              <span class="fa fa-home form-control-feedback"></span>
            </div>
          </div>

        <div class="form-row">
          <div class="col">
            <div class="form-group icon_form">
              <textarea name="mensagem" cols="25" rows="5" class="form-control form-control-lg fundo-form" placeholder="MENSAGEM"></textarea>
              <span class="fa fa-pencil form-control-feedback"></span>
            </div>
          </div>
        </div>


        <div class="col-12 text-right  top15 padding0">
          <button type="submit" class="btn btn_formulario_orcamento" name="btn_contato">
            ENVIAR
          </button>
        </div>

      </div>

    </div>
    <!--  ==============================================================  -->
    <!-- FORMULARIO-->
    <!--  ==============================================================  -->
  </form>

</div>

</div>




<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->

</body>

</html>

<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->
<?php require_once('./includes/js_css.php') ?>
<!-- ======================================================================= -->
<!-- js css    -->
<!-- ======================================================================= -->


<?php
//  VERIFICO SE E PARA CADASTRAR A SOLICITACAO
if(isset($_POST[nome])){

  //  CADASTRO OS PRODUTOS SOLICITADOS
  for($i=0; $i < count($_POST[qtd]); $i++){
    $dados = $obj_site->select_unico("tb_produtos", "idproduto", $_POST[idproduto][$i]);

    $produtos .= "
    <tr>
    <td><p>". $_POST[qtd][$i] ."</p></td>
    <td><p>". utf8_encode($dados[titulo]) ."</p></td>
    </tr>
    ";
  }

  //  CADASTRO OS SERVICOS SOLICITADOS
  for($i=0; $i < count($_POST[qtd_servico]); $i++){
    $dados = $obj_site->select_unico("tb_tipos_servicos", "idtiposervico", $_POST[idtiposervico][$i]);

    $produtos .= "
    <tr>
    <td><p>". $_POST[qtd_servico][$i] ."</p></td>
    <td><p>". utf8_encode($dados[titulo]) ."</p></td>
    </tr>
    ";
  }




  //  ENVIANDO A MENSAGEM PARA O CLIENTE
  $texto_mensagem = "
  O seguinte cliente fez uma solicitação pelo site. <br />

  Nome: $_POST[nome] <br />
  Email: $_POST[email] <br />
  Telefone: $_POST[telefone] <br />
  Localidade: $_POST[localidade] <br />
  Mensagem: <br />
  ". nl2br($_POST[mensagem]) ." <br />

  <br />
  <h2> Produtos selecionados:</h2> <br />

  <table width='100%' border='0' cellpadding='5' cellspacing='5'>
  <tr>
  <td><h4>QTD</h4></td>
  <td><h4>PRODUTO</h4></td>
  </tr>
  $produtos
  </table>

  ";


  if (Util::envia_email($config[email], utf8_decode("$_POST[nome] solicitou contato pelo site"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]),utf8_decode($_POST[email]))) {
    Util::envia_email($config[email_copia], utf8_decode("$_POST[nome] solicitou um orçamento"), utf8_decode($texto_mensagem), utf8_decode($_POST[nome]),utf8_decode($_POST[email]));
    unset($_SESSION[solicitacoes_produtos]);
    unset($_SESSION[solicitacoes_servicos]);
    unset($_SESSION[piscinas_vinil]);
    Util::alert_bootstrap("Orçamento enviado com sucesso. Em breve entraremos em contato.");
  }else{
    Util::alert_bootstrap("Houve um erro ao enviar sua mensagem, por favor tente novamente.");
  }

}

?>
