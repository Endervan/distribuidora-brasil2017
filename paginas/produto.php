<?php


// INTERNA
$url = Url::getURL(1);


if(!empty($url))
{
  $complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0)
{
  Util::script_location(Util::caminho_projeto()."/produtos");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];


?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>



<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",3) ?>
<style>
.bg-interna{
  background:  #f2f2f2 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>)top center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->




  <!-- ======================================================================= -->
  <!--PRODUTO DENTRO-->
  <!-- ======================================================================= -->
  <div class='container'>
    <div class="row top160">

      <!-- ======================================================================= -->
      <!-- SLIDER CATEGORIA -->
      <!-- ======================================================================= -->
      <div class="col-5 slider_prod_geral">

        <!-- Place somewhere in the <body> of your page -->
        <div id="slider" class="flexslider">
          <span>Clique na imagem para Ampliar</span>
          <ul class="slides slider-prod">
            <?php
            $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
            if(mysql_num_rows($result) > 0)
            {
              while($row = mysql_fetch_array($result)){
                ?>
                <li class="zoom">
                  <a href="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo $row[imagem]; ?>" class="group4">
                    <img src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" class="input100" />
                  </a>
                </li>
                <?php
              }
            }
            ?>
            <!-- items mirrored twice, total of 12 -->
          </ul>
        </div>
        <div id="carousel" class="flexslider">
          <ul class="slides slider-prod-tumb">
            <?php
            $result = $obj_site->select("tb_galerias_produtos", "AND id_produto = '$dados_dentro[0]' ");
            if(mysql_num_rows($result) > 0)
            {
              while($row = mysql_fetch_array($result)){
                ?>
                <li>
                  <img src="<?php echo Util::caminho_projeto() ?>/uploads/tumb_<?php Util::imprime($row[imagem]); ?>" />

                </li>
                <?php
              }
            }
            ?>
            <!-- items mirrored twice, total of 12 -->
          </ul>
        </div>
      </div>

      <div class="col-7 empresa_titulo">
        <div class="bottom25">
          <h2 class="text-uppercase"><?php Util::imprime($dados_dentro[titulo]); ?></h2>
        </div>

        <div class="bottom25">
          <h2 class="text-uppercase"><span><?php Util::imprime($dados_dentro[subtitulo]); ?></span></h2>
        </div>


        <div class="row">


          <div class="col-7">
            <div class="dropdown w-100 show">
              <a class="btn btn_topo" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <img  class="" src="<?php echo Util::caminho_projeto(); ?>/imgs/atentimento.png" alt="" />
              </a>

              <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">

                <!-- ======================================================================= -->
                <!-- nossas lojas  -->
                <!-- ======================================================================= -->
                <div class="col-12 pg20">
                  <h2 class="text-right">
                    <?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?> /

                    <?php if (!empty($config[telefone2])): ?>
                      <?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
                    <?php endif; ?>

                    <?php if (!empty($config[telefone3])): ?>
                      <?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
                    <?php endif; ?>

                    <?php if (!empty($config[telefone4])): ?>
                      <?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>
                    <?php endif; ?>

                  </h2>
                </div>

                <!-- ======================================================================= -->
                <!-- nossas lojas  -->
                <!-- ======================================================================= -->
              </div>
            </div>
          </div>

          <div class="col-5">
            <a href="javascript:void(0);"  title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($dados_dentro[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($dados_dentro[0]) ?>, 'produto'">
              <img class="w-100" src="<?php echo Util::caminho_projeto(); ?>/imgs/btn_ocamento_produto.png" alt="">
            </a>
          </div>
        </div>

        <div class="col-12">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 8);?>
          <div class="top60 desc_prod">
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>
        </div>




      </div>

      <div class="col-12 top50">
        <div class="">
          <h3>DESCRIÇÃO COMPLETA</h3>
          <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_parceiros.png" alt="" >
        </div>

        <div class="top25">
          <p><?php Util::imprime($dados_dentro[descricao]); ?></p>
        </div>
      </div>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!--PRODUTO DENTRO-->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!-- VEJA TAMBEM    -->
  <!-- ======================================================================= -->
  <div class="container top90">
    <div class="row">

      <div class="col-12">
        <h3>VEJA TAMBÉM</h3>
        <img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_parceiros.png" alt="" >
      </div>

      <?php
      $i = 0;
      $result = $obj_site->select("tb_produtos","limit 4");
      if(mysql_num_rows($result) == 0){
        echo "<h2 class='bg-info top25' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
      }else{
        while ($row = mysql_fetch_array($result))
        {
          $result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
          $row_categoria = mysql_fetch_array($result_categoria);
          ?>

          <div class="col-3 top30 pull-right">

            <div class="card">
              <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
                <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 269, 278, array("class"=>"input100", "alt"=>"$row[titulo]")) ?>
              </a>
              <div class="card-body">
                <div class="top15">  <h2><span><?php Util::imprime($row_categoria[titulo]); ?></span></h2></div>
                <div class="top25 desc_titulo_home">  <h1 ><?php Util::imprime($row[titulo],60); ?></h1></div>
                <div class="top10">  </div>
                <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais">SAIBA MAIS <i class="fa fa-plus-circle left10" aria-hidden="true"></i></a>
              </div>

              <div class="produto_hover_home">
                <div class="">
                  <a href="<?php echo Util::caminho_projeto() ?>/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_hover">SAIBA MAIS <i class="fa fa-plus-circle left10" aria-hidden="true"></i></a>
                </div>
                <div class="">
                  <a href="javascript:void(0);" class="btn_produtos_home top20" title="SOLICITAR UM ORÇAMENTO" onclick="add_solicitacao(<?php Util::imprime($row[0]) ?>, 'produto')" id="btn_add_solicitacao_<?php Util::imprime($row[0]) ?>, 'produto'">
                    <img src="<?php echo Util::caminho_projeto() ?>/imgs/btn_orcamento.jpg" alt="">
                  </a>
                </div>
              </div>

            </div>
          </div>

          <?php
          if($i == 3){
            echo '<div class="clearfix"></div>';
            $i = 0;
          }else{
            $i++;
          }

        }
      }
      ?>


    </div>
  </div>
  <!-- ======================================================================= -->
  <!-- VEJA TAMBEM    -->
  <!-- ======================================================================= -->

  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/rodape.php') ?>
  <!-- ======================================================================= -->
  <!-- rodape    -->
  <!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>


<script>
$(window).load(function() {
  // The slider being synced must be initialized first
  $('#carousel').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    itemWidth: 100,
    itemMargin: 5,
    asNavFor: '#slider'
  });

  $('#slider').flexslider({
    animation: "slide",
    controlNav: false,
    animationLoop: false,
    slideshow: false,
    sync: "#carousel"
  });
});


</script>
