<?php

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 9);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
  <?php require_once('./includes/head.php'); ?>

</head>


<!--  ==============================================================  -->
<!-- background -->
<!--  ==============================================================  -->
<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna",4) ?>
<style>
.bg-interna{
  background:  #f2f2f2 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top  center no-repeat;
}
</style>

<body class="bg-interna">


  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->
  <?php require_once('./includes/topo.php') ?>
  <!-- ======================================================================= -->
  <!-- topo    -->
  <!-- ======================================================================= -->



  <!-- ======================================================================= -->
  <!--  titulo geral -->
  <!-- ======================================================================= -->
  <div class="container">
    <div class="row top90">
      <div class="col-12">
        <h4>CONFIRA TODAS AS</h4>
        <h4><span>NOSSAS DICAS</span></h4>
      </div>
    </div>
  </div>
</div>
<!-- ======================================================================= -->
<!--  titulo -->
<!-- ======================================================================= -->




<!-- ======================================================================= -->
<!-- DICAS    -->
<!-- ======================================================================= -->
<div class="container top40">
  <div class="row">

    <?php
    $result = $obj_site->select("tb_dicas");
    if(mysql_num_rows($result) > 0){
      while($row = mysql_fetch_array($result)){
        $i=0;
        ?>

        <div class="col-4 top30">
          <div class="card dicas">
            <a href="<?php echo Util::caminho_projeto() ?>/dica/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
              <?php $obj_site->redimensiona_imagem("../uploads/$row[imagem]", 370, 224, array("class"=>"card-img-top pg10", "alt"=>"$row[titulo]")) ?>
              <div class="card-body">
                <h5 class="card-title"><?php Util::imprime($row[titulo]); ?></h5>
                <div class="desc_dicas">
                  <p class="card-text"><?php Util::imprime($row[descricao]); ?></p>
                </div>
              </div>
            </a>
          </div>
        </div>
        <?php
        if ($i==2) {
          echo "<div class='clearfix'>  </div>";
        }else {
          $i++;
        }
      }
    }
    ?>




  </div>
</div>
<!-- ======================================================================= -->
<!-- DICAS    -->
<!-- ======================================================================= -->


<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->
<?php require_once('./includes/rodape.php') ?>
<!-- ======================================================================= -->
<!-- rodape    -->
<!-- ======================================================================= -->



</body>

</html>


<?php require_once('./includes/js_css.php') ?>
