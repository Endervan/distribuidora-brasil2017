
<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// INTERNA
$url =$_GET[get1];

if(!empty($url)){
	$complemento = "AND url_amigavel = '$url'";
}

$result = $obj_site->select("tb_produtos", $complemento);

if(mysql_num_rows($result)==0){
	Util::script_location(Util::caminho_projeto()."/mobile/produtos/");
}

$dados_dentro = mysql_fetch_array($result);
// BUSCA META TAGS E TITLE
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];
?>

<!doctype html>
<html amp lang="pt-br">
<head>
	<?php require_once("../includes/head.php"); ?>
	<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

	<style amp-custom>
	<?php require_once("../css/geral.css"); ?>
	<?php require_once("../css/topo_rodape.css"); ?>
	<?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



	<?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 12); ?>
	.bg-interna{
		background: #f2f2f2 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
	}
	</style>

	<script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
	<script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>
	<script async custom-element="amp-lightbox" src="https://cdn.ampproject.org/v0/amp-lightbox-0.1.js"></script>
	<script async custom-element="amp-image-lightbox" src="https://cdn.ampproject.org/v0/amp-image-lightbox-0.1.js"></script>


</head>





<body class="bg-interna">


	<?php
	$voltar_para = 'produtos'; // link de volta, exemplo produtos, dicas, servicos etc
	require_once("../includes/topo.php")
	?>


	<div class="row">
		<div class="col-12 localizacao-pagina-produtos text-center top50">
			<h2 class="text-uppercase"><span><?php Util::imprime($dados_dentro[subtitulo]); ?></span></h2>
			<h1><?php echo Util::imprime($dados_dentro[titulo]) ?></h1>

		</div>
	</div>



	<div class="row">
		<div class="col-12 text-center top50">

			<amp-carousel id="carousel-with-preview"
			width="450"
			height="314"
			layout="responsive"
			type="slides"
			autoplay
			delay="4000"
			>

			<?php
			$i = 0;
			$result = $obj_site->select("tb_galerias_produtos", "and id_produto = $dados_dentro[idproduto]");
			if (mysql_num_rows($result) > 0) {
				while($row = mysql_fetch_array($result)){
					?>


					<amp-img
					on="tap:lightbox1"
					role="button"
					tabindex="0"

					src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
					layout="responsive"
					width="450"
					height="377"
					alt="<?php echo Util::imprime($row[titulo]) ?>">

				</amp-img>
				<?php
			}
			$i++;
		}
		?>

	</amp-carousel>

	<amp-image-lightbox id="lightbox1"layout="nodisplay">	</amp-image-lightbox>




	<div class="carousel-preview text-center">

		<?php
		$i = 0;
		$result = $obj_site->select("tb_galerias_produtos", "and id_produto = $dados_dentro[idproduto]");
		if (mysql_num_rows($result) > 0) {
			while($row = mysql_fetch_array($result)){
				?>

				<button on="tap:carousel-with-preview.goToSlide(index=<?php echo $i++; ?>)">

					<amp-img class="col-3"
					src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
					width="40"
					height="40"
					alt="<?php echo Util::imprime($row[titulo]) ?>">
				</amp-img>
			</button>

			<?php
		}
		$i++;
	}
	?>
</div>

</div>

</div>

<div class="row">

	<div class="col-10 top50">

		<a class="ampstart-btn caps m2"
		on="tap:my-lightbox"
		role="a"
		tabindex="0">
		<amp-img
		src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/btn_atentimento.png"
		width="210"
		height="50"
		layout="responsive"
		alt="<?php echo Util::imprime($row[titulo]) ?>">
	</amp-img>
</a>

<amp-lightbox id="my-lightbox" layout="nodisplay">
	<div class="lightbox" role="a" tabindex="0">
		<!-- ======================================================================= -->
		<!-- telefones  -->
		<!-- ======================================================================= -->
		<div>

			<button class="btn btn_fechar" on="tap:my-lightbox.close">Fechar</button>
			<div class="media top35">
				<div class="media-left media-middle">
					<i class="fa fa-phone"></i>
				</div>

				<div class="media-body">

					<!-- ======================================================================= -->
					<!-- nossas lojas  -->
					<!-- ======================================================================= -->
					<div class="">
						<h2 class="text-right">
							<a href="tel:+55<?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>">
								<?php Util::imprime($config[ddd1]); ?> <?php Util::imprime($config[telefone1]); ?>
							</a>

							/ <?php if (!empty($config[telefone2])): ?>
								<a href="tel:+55<?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>">
									<i class="fa fa-whatsapp"></i>
									<?php Util::imprime($config[ddd2]); ?> <?php Util::imprime($config[telefone2]); ?>
								</a>
							<?php endif; ?>

							<?php if (!empty($config[telefone3])): ?>
								<a href="tel:+55<?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>">
									/ <i class="fa fa-phone"></i>
									<?php Util::imprime($config[ddd3]); ?> <?php Util::imprime($config[telefone3]); ?>
								</a>
							<?php endif; ?>

							<?php if (!empty($config[telefone4])): ?>
								<a href="tel:+55<?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>">
									<?php Util::imprime($config[ddd4]); ?> <?php Util::imprime($config[telefone4]); ?>
								</a>
							<?php endif; ?>

						</h2>
					</div>

					<!-- ======================================================================= -->
					<!-- nossas lojas  -->
					<!-- ======================================================================= -->
				</div>
			</div>
			<!-- ======================================================================= -->
			<!-- telefones  -->
			<!-- ======================================================================= -->
		</div>
	</div>

</amp-lightbox>

</div>

<div class="col-8">
	<a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/index.php?action=add&id=<?php echo $dados_dentro[0] ?>&tipo_orcamento=produto" title="SOLICITAR UM ORÇAMENTO">
		<amp-img
		src="<?php echo Util::caminho_projeto() ?>/mobile/imgs/btn_orcamento.png"
		width="210"
		height="57"
		layout="responsive"
		alt="<?php echo Util::imprime($row[titulo]) ?>">
	</amp-img>
</a>
</div>

</div>







<div class="row">
	<div class="col-12">
		<?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 8);?>
		<div class="top20 bottom25 dicas_dentro">
			<p><?php Util::imprime($row[descricao]); ?></p>
		</div>
	</div>

	<div class="col-12">
		<h3>DESCRIÇÃO COMPLETA</h3>
		<amp-img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_parceiros.png" height="4" width="183"></amp-img>
	</div>

	<div class="col-12 dicas_dentro ">
		<div class="top30">	<p><?php echo Util::imprime($dados_dentro[descricao]) ?></p></div>
	</div>
</div>


<!-- ======================================================================= -->
<!-- VEJA TAMBEM    -->
<!-- ======================================================================= -->
<div class="row bottom50">

	<div class="col-12">
		<h3>VEJA TAMBÉM</h3>
		<amp-img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_parceiros.png" height="4" width="183"></amp-img>
	</div>

	<?php
	$i = 0;
	$result = $obj_site->select("tb_produtos","limit 2");
	if(mysql_num_rows($result) == 0){
		echo "<h2 class='bg-info top25' style='padding: 20px;'>Nenhum produto encontrado.</h2>";
	}else{
		while ($row = mysql_fetch_array($result))
		{
			$result_categoria = $obj_site->select("tb_categorias_produtos","AND idcategoriaproduto = ".$row[id_categoriaproduto]);
			$row_categoria = mysql_fetch_array($result_categoria);
			?>

			<div class="col-6 top5 produtos">

				<div class="card pt15">
					<a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="<?php Util::imprime($row[titulo]); ?>">
						<amp-img
						layout="responsive"
						height="250" width="426"
						src="<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($row[imagem]); ?>" alt="<?php Util::imprime($row[titulo]); ?>" >
					</amp-img>
				</a>
				<div class="card-body">
					<div class="top5">  <h2><span><?php Util::imprime($row_categoria[titulo]); ?></span></h2></div>
					<div class="top10 desc_titulo_home">  <h1 ><?php Util::imprime($row[titulo],60); ?></h1></div>
				</div>

				<a href="<?php echo Util::caminho_projeto() ?>/mobile/produto/<?php Util::imprime($row[url_amigavel]); ?>" title="Saiba Mais" class="btn btn_saiba_mais">SAIBA MAIS <i class="fa fa-plus-circle left10" aria-hidden="true"></i></a>

				<div class="top10">
					<a href="<?php echo Util::caminho_projeto() ?>/mobile/orcamento/index.php?action=add&id=<?php echo $row[0] ?>&tipo_orcamento=produto" title="SOLICITAR UM ORÇAMENTO">
						<amp-img
						src="<?php echo Util::caminho_projeto() ?>/imgs/btn_orcamento.jpg"
						width="210"
						height="50"
						layout="responsive"
						alt="<?php echo Util::imprime($row[titulo]) ?>">
					</amp-img>
				</a>
			</div>


		</div>
	</div>

	<?php
	if($i == 1){
		echo '<div class="clearfix"></div>';
		$i = 0;
	}else{
		$i++;
	}

}
}
?>


</div>
<!-- ======================================================================= -->
<!-- VEJA TAMBEM    -->
<!-- ======================================================================= -->



<?php require_once("../includes/rodape.php") ?>

</body>



</html>
