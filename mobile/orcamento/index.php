<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 6);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];



function adiciona($id, $tipo_orcamento){
  //  VERIFICO SE O TIPO DE SOLICITACAO E DE SERVICO OU PRODUTO
  switch($tipo_orcamento){
    // case "servico":
    // //	VERIFICO SE NAO TEM JA ADICIONADO
    // if(count($_SESSION[solicitacoes_servicos] > 0)):
    //   if (!@in_array($id, $_SESSION[solicitacoes_servicos])) :
    //     $_SESSION[solicitacoes_servicos][] = $id;
    //   endif;
    // else:
    //   $_SESSION[solicitacoes_servicos][] = $id;
    // endif;
    // break;
    case "produto":
        //	VERIFICO SE NAO TEM JA ADICIONADO
        if(count($_SESSION[solicitacoes_produtos] > 0)):
                if (!@in_array($id, $_SESSION[solicitacoes_produtos])) :
                        $_SESSION[solicitacoes_produtos][] = $id;
                endif;
        else:
                $_SESSION[solicitacoes_produtos][] = $id;
        endif;
    break;
  }
}



function excluir($id, $tipo_orcamento){
  switch ($tipo_orcamento) {
    case 'produto':
        unset($_SESSION[solicitacoes_produtos][$id]);
        sort($_SESSION[solicitacoes_produtos]);
    // break;
    // case 'servico';
    // unset($_SESSION[solicitacoes_servicos][$id]);
    // sort($_SESSION[solicitacoes_servicos]);
    // break;
  }
}




//  EXCLUI OU ADD UM ITEM
if($_GET[action] != ''){
  //  SELECIONO O TIPO
  switch($_GET[action]){
    case "add":
    adiciona($_GET[id], $_GET[tipo_orcamento]);
    break;
    case "del":
    excluir($_GET[id], $_GET[tipo_orcamento]);
    break;
  }
}


?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-form" src="https://cdn.ampproject.org/v0/amp-form-0.1.js"></script>
  <script async custom-template="amp-mustache" src="https://cdn.ampproject.org/v0/amp-mustache-0.1.js"></script>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>


  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>

  form.amp-form-submit-success [submit-success],
  form.amp-form-submit-error [submit-error]{
    margin-top: 16px;
  }
  form.amp-form-submit-success [submit-success] {
    color: #fff;
  }
  form.amp-form-submit-error [submit-error] {
    color: red;
  }
  form.amp-form-submit-success > input{
  display: none;
  }




  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 18); ?>
  .bg-interna{
    background: #f2f2f2 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 67px center  no-repeat;
  }
  </style>




</head>





<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>


  <div class="row">
    <!-- ======================================================================= -->
    <!--  titulo geral -->
    <!-- ======================================================================= -->
    <div class="col-12 titulo_emp">
      <div class="">
        <h2>SERÁ UM PRAZER ATENDÊ-LO</h2>
        <h1>ENVIE SEU ORCAMENTO</h1>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!--  titulo -->
    <!-- ======================================================================= -->

  </div>





  <div class="row bottom50">
    <div class="col-12 text-center">

      <form method="post" class="p2" action-xhr="envia.php" target="_top">

        <?php require_once('../includes/lista_itens_orcamento.php'); ?>

        <div class="lista-produto-titulo top20">
          <h1 class="">CONFIRME SEUS DADOS</h1>
        </div>

        <div class="ampstart-input inline-block relative m0 p0 mb3 orcamento-input top20">
          <input type="text" class="input-form input100 block border-none p0 m0" name="nome" placeholder="NOME" required>
          <input type="email" class="input-form input100 block border-none p0 m0" name="email" placeholder="EMAIL" required>
          <input type="tel" class="input-form input100 block border-none p0 m0" name="telefone" placeholder="TELEFONE" required>
          <input type="text" class="input-form input100 block border-none p0 m0" name="localidade" placeholder="LOCALIDADE" required>
          <textarea name="mensagem" placeholder="MENSAGEM" class="input-form input100 campo-textarea" ></textarea>
        </div>

          <input type="submit" value="ENVIAR" class="ampstart-btn caps btn btn_formulario text-center bottom50">


        <div submit-success>
          <template type="amp-mustache">
            Email enviado com sucesso! {{name}} entraremos em contato o mais breve possível.
          </template>
        </div>

        <div submit-error>
          <template type="amp-mustache">
            Houve um erro, {{name}} por favor tente novamente.
          </template>
        </div>


      </form>
    </div>
  </div>








  <?php require_once("../includes/rodape.php") ?>

</body>



</html>
