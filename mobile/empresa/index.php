<?php
require_once("../../class/Include.class.php");
$obj_site = new Site();

// BUSCA META TAGS E TITLE
$dados_dentro = $obj_site->select_unico("tb_seo", "idseo", 2);
$description = $dados_dentro[description_google];
$keywords = $dados_dentro[keywords_google];
$titulo_pagina = $dados_dentro[title_google];

?>
<!doctype html>
<html amp lang="pt-br">
<head>
  <?php require_once("../includes/head.php"); ?>
  <script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

  <style amp-custom>
  <?php require_once("../css/geral.css"); ?>
  <?php require_once("../css/topo_rodape.css"); ?>
  <?php require_once("../css/paginas.css");  //  ARQUIVO DA PAGINA ?>



  <?php $banner = $obj_site->select_unico("tb_banners_internas", "idbannerinterna", 10); ?>
  .bg-interna{
    background: #f2f2f2 url(<?php echo Util::caminho_projeto() ?>/uploads/<?php Util::imprime($banner[imagem]); ?>) top 74px center  no-repeat;
  }
  </style>


  <script async custom-element="amp-carousel" src="https://cdn.ampproject.org/v0/amp-carousel-0.1.js"></script>
  <script async custom-element="amp-bind" src="https://cdn.ampproject.org/v0/amp-bind-0.1.js"></script>


</head>





<body class="bg-interna">


  <?php require_once("../includes/topo.php") ?>

  <div class="row">

    <!-- ======================================================================= -->
    <!--  titulo geral -->
    <!-- ======================================================================= -->
    <div class="col-12 titulo_emp">
      <div class="">
        <h2>SAIBA MAIS SOBRE A</h2>
        <h1 class="text-capitalize">DISTRIBUIDORA BRASIL</h1>
      </div>
    </div>
    <!-- ======================================================================= -->
    <!--  titulo -->
    <!-- ======================================================================= -->

    <div class="col-12 desc-empresa">
      <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 2);?>
      <p><span><?php Util::imprime($row[descricao]); ?></span></p>
    </div>

  </div>

  <div class="row">

    <div class="col-12 text-center facildades_empresa top10">
      <div class="card">
        <amp-img class="pg10 top20" src="<?php echo Util::caminho_projeto(); ?>/imgs/falicidades01.png" alt="A EMPRESA" height="90" width="90"></amp-img>
        <div class="card-body">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 3);?>
          <div class="">
            <h1><span><?php Util::imprime($row[titulo]); ?></span></h1>
          </div>
          <div class="top20 bottom10">
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 text-center facildades_empresa top10">
      <div class="card">
        <amp-img class="pg10 top20" src="<?php echo Util::caminho_projeto(); ?>/imgs/falicidades02.png" alt="A EMPRESA" height="90" width="90"></amp-img>
        <div class="card-body">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 4);?>
          <div class="">
            <h1><span><?php Util::imprime($row[titulo]); ?></span></h1>
          </div>
          <div class="top20 bottom10">
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 text-center facildades_empresa top10">
      <div class="card">
        <amp-img class="pg10 top20" src="<?php echo Util::caminho_projeto(); ?>/imgs/falicidades06.png" alt="A EMPRESA" height="90" width="90"></amp-img>
        <div class="card-body">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 7);?>
          <div class="">
            <h1><span><?php Util::imprime($row[titulo]); ?></span></h1>
          </div>
          <div class="top20 bottom10">
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 text-center facildades_empresa top10">
      <div class="card">
        <amp-img class="pg10 top20" src="<?php echo Util::caminho_projeto(); ?>/imgs/falicidades05.png" alt="A EMPRESA" height="90" width="90"></amp-img>
        <div class="card-body">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 5);?>
          <div class="">
            <h1><span><?php Util::imprime($row[titulo]); ?></span></h1>
          </div>
          <div class="top20 bottom10">
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>
        </div>
      </div>
    </div>

    <div class="col-12 text-center facildades_empresa top10">
      <div class="card">
        <amp-img class="pg10 top20" src="<?php echo Util::caminho_projeto(); ?>/imgs/falicidades04.png" alt="A EMPRESA" height="90" width="90"></amp-img>
        <div class="card-body">
          <?php $row = $obj_site->select_unico("tb_empresa", "idempresa", 6);?>
          <div class="">
            <h1><span><?php Util::imprime($row[titulo]); ?></span></h1>
          </div>
          <div class="top20 bottom10">
            <p><?php Util::imprime($row[descricao]); ?></p>
          </div>
        </div>
      </div>
    </div>



  </div>






  <div class="row">

    <div class="col-12 top40">
      <h3>NOSSOS PARCEIROS</h3>
      <amp-img src="<?php echo Util::caminho_projeto(); ?>/imgs/barra_parceiros.png" height="4" width="183"></amp-img>
    </div>

    <div class="col-12 top10 clientes_emp bottom50">

      <amp-carousel id="carousel-with-preview"
      width="200"
      height="112"
      layout="responsive"
      type="slides"
      autoplay
      delay="4000">

      <?php
      $i = 0;
      $result = $obj_site->select("tb_clientes");
      if (mysql_num_rows($result) > 0) {
        while($row = mysql_fetch_array($result)){
          ?>


          <amp-img
          src="<?php echo Util::caminho_projeto() ?>/uploads/<?php echo Util::imprime($row[imagem]) ?>"
          width="200"
          height="112"
          layout="responsive"
          alt="<?php echo Util::imprime($row[titulo]) ?>">
        </amp-img>
        <?php
      }
      $i++;
    }
    ?>
  </amp-carousel>



</div>
</div>




<?php require_once("../includes/rodape.php") ?>

</body>



</html>
